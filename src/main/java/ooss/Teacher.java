package ooss;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Teacher extends Person {

    private final List<Klass> classes = new ArrayList<>();

    public Teacher(Integer id, String name, Integer age) {
        super(id, name, age);
    }

    @Override
    public String introduce() {
        String intro = super.introduce() + " I am a teacher.";
        String classNumbers = classes.stream()
            .map(klass -> klass.getNumber().toString())
            .collect(Collectors.joining(", "));

        return classes.isEmpty()
            ? intro
            : intro + " I teach Class " + classNumbers + ".";
    }

    public void assignTo(Klass klass) {
        if (!classes.contains(klass)) {
            classes.add(klass);
        }
    }

    public boolean belongsTo(Klass klass) {
        return classes.contains(klass);
    }

    public boolean isTeaching(Student tom) {
        return classes.stream().anyMatch(tom::isIn);
    }

    @Override
    public void update(Klass klass) {
        if (belongsTo(klass)) {
            System.out.printf(
                "I am %s, teacher of Class %d. I know %s become Leader.",
                this.getName(),
                klass.getNumber(),
                klass.getLeader().getName()
            );
        }
    }
}
