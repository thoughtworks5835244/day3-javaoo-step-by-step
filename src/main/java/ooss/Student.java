package ooss;

public class Student extends Person {

    private Klass klass;

    public Student(Integer id, String name, Integer age) {
        super(id, name, age);
    }

    @Override
    public String introduce() {
        String intro = super.introduce() + " I am a student.";
        if (klass == null) return intro;

        String leaderIntro = klass.isLeader(this)
            ? " I am the leader of class " + klass.getNumber() + "."
            : " I am in class " + klass.getNumber() + ".";

        return intro + leaderIntro;
    }

    public void join(Klass klass) {
        this.klass = klass;
    }

    public boolean isIn(Klass klass) {
        return this.klass != null && this.klass.equals(klass);
    }

    @Override
    public void update(Klass klass) {
        if (this.klass.equals(klass)) {
            System.out.printf(
                "I am %s, student of Class %d. I know %s become Leader.",
                this.getName(),
                this.klass.getNumber(),
                this.klass.getLeader().getName()
            );
        }
    }
}
