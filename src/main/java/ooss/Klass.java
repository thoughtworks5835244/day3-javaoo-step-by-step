package ooss;

import java.util.ArrayList;
import java.util.List;

public class Klass {

    private final Integer number;
    private final List<Person> members = new ArrayList<>();
    private Student leader;

    public Klass(Integer number) {
        this.number = number;
    }

    public Student getLeader() {
        return leader;
    }

    public Integer getNumber() {
        return number;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Klass) {
            return this.number.equals(((Klass) obj).number);
        }
        return false;
    }

    public void assignLeader(Student student) {
        if (!student.isIn(this)) {
            System.out.println("It is not one of us.");
        } else {
            this.leader = student;
            members.forEach(m -> m.update(this));
        }
    }

    public boolean isLeader(Student student) {
        return this.leader != null && this.leader.equals(student);
    }

    public void attach(Person person) {
        if (!members.contains(person)) {
            members.add(person);
        }
    }
}
