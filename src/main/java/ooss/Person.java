package ooss;

public class Person {

    private Integer id;
    private String name;
    private Integer age;

    public String getName() {
        return name;
    }

    public Person(Integer id, String name, Integer age) {
        this.id = id;
        this.name = name;
        this.age = age;
    }

    public String introduce() {
        return String.format("My name is %s. I am %d years old.", name, age);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Person) {
            return this.id.equals(((Person) obj).id);
        }
        return false;
    }

    public void update(Klass klass) {
    }
}
